package com.pacifico.solicitud.digital.bean;

import java.math.BigDecimal;
import java.util.List;

public class CotizacionPlan {
	
	private String codigoProducto;
	private String codigoPlan;
	private String descripcionPlan;
	private String codigoRevision;
	private Fraccionamiento fraccionamiento;
	private Asegurados asegurados;
	private Observacion observacion;
	private Integer monedaCoberturaMaxima;
	private BigDecimal coberturaMaxima;
	private String coberturaClinicas; 
	private PlanGrupoBeneficio planGrupoBeneficio;
	
	private int IdCotizacionPlan;
	private int IdPlan;
	private String Descripcion;
	private int IdTipoDescuento;
	private List<CotizacionAsegurado> CotizacionAsegurado;
	private List<CotizacionFraccionamiento> CotizacionFraccionamiento;
	
	
	
	public List<CotizacionAsegurado> getCotizacionAsegurado() {
		return CotizacionAsegurado;
	}
	public void setCotizacionAsegurado(List<CotizacionAsegurado> cotizacionAsegurado) {
		CotizacionAsegurado = cotizacionAsegurado;
	}
	public List<CotizacionFraccionamiento> getCotizacionFraccionamiento() {
		return CotizacionFraccionamiento;
	}
	public void setCotizacionFraccionamiento(List<CotizacionFraccionamiento> cotizacionFraccionamiento) {
		CotizacionFraccionamiento = cotizacionFraccionamiento;
	}
	public int getIdCotizacionPlan() {
		return IdCotizacionPlan;
	}
	public void setIdCotizacionPlan(int idCotizacionPlan) {
		IdCotizacionPlan = idCotizacionPlan;
	}
	public int getIdPlan() {
		return IdPlan;
	}
	public void setIdPlan(int idPlan) {
		IdPlan = idPlan;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public int getIdTipoDescuento() {
		return IdTipoDescuento;
	}
	public void setIdTipoDescuento(int idTipoDescuento) {
		IdTipoDescuento = idTipoDescuento;
	}
	public String getCodigoProducto() {
		return codigoProducto;
	}
	public void setCodigoProducto(String codigoProducto) {
		this.codigoProducto = codigoProducto;
	}
	public String getCodigoPlan() {
		return codigoPlan;
	}
	public void setCodigoPlan(String codigoPlan) {
		this.codigoPlan = codigoPlan;
	}
	public String getDescripcionPlan() {
		return descripcionPlan;
	}
	public void setDescripcionPlan(String descripcionPlan) {
		this.descripcionPlan = descripcionPlan;
	}
	public String getCodigoRevision() {
		return codigoRevision;
	}
	public void setCodigoRevision(String codigoRevision) {
		this.codigoRevision = codigoRevision;
	}
	public Fraccionamiento getFraccionamiento() {
		return fraccionamiento;
	}
	public void setFraccionamiento(Fraccionamiento fraccionamiento) {
		this.fraccionamiento = fraccionamiento;
	}
	public Asegurados getAsegurados() {
		return asegurados;
	}
	public void setAsegurados(Asegurados asegurados) {
		this.asegurados = asegurados;
	}
	public Observacion getObservacion() {
		return observacion;
	}
	public void setObservacion(Observacion observacion) {
		this.observacion = observacion;
	}
	public Integer getMonedaCoberturaMaxima() {
		return monedaCoberturaMaxima;
	}
	public void setMonedaCoberturaMaxima(Integer monedaCoberturaMaxima) {
		this.monedaCoberturaMaxima = monedaCoberturaMaxima;
	}
	public BigDecimal getCoberturaMaxima() {
		return coberturaMaxima;
	}
	public void setCoberturaMaxima(BigDecimal coberturaMaxima) {
		this.coberturaMaxima = coberturaMaxima;
	}
	public String getCoberturaClinicas() {
		return coberturaClinicas;
	}
	public void setCoberturaClinicas(String coberturaClinicas) {
		this.coberturaClinicas = coberturaClinicas;
	}
	public PlanGrupoBeneficio getPlanGrupoBeneficio() {
		return planGrupoBeneficio;
	}
	public void setPlanGrupoBeneficio(PlanGrupoBeneficio planGrupoBeneficio) {
		this.planGrupoBeneficio = planGrupoBeneficio;
	}
	
}
