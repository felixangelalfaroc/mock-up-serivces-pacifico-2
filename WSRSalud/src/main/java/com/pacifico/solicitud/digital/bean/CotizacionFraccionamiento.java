package com.pacifico.solicitud.digital.bean;

import java.util.List;

public class CotizacionFraccionamiento {
private String CodPlanFrac;
private String DesPlanFrac;
private String VerPlanFrac;
private String IndDebito;
private String IndAsesor;
private double PrimaTotalAnual;
private double PrimaTotalFraccionada;
private boolean FlagSeleccionado;
private List<FraccionamientoAsegurado> FraccionamientoAsegurado;
public String getCodPlanFrac() {
	return CodPlanFrac;
}
public void setCodPlanFrac(String codPlanFrac) {
	CodPlanFrac = codPlanFrac;
}
public String getDesPlanFrac() {
	return DesPlanFrac;
}
public void setDesPlanFrac(String desPlanFrac) {
	DesPlanFrac = desPlanFrac;
}
public String getVerPlanFrac() {
	return VerPlanFrac;
}
public void setVerPlanFrac(String verPlanFrac) {
	VerPlanFrac = verPlanFrac;
}
public String getIndDebito() {
	return IndDebito;
}
public void setIndDebito(String indDebito) {
	IndDebito = indDebito;
}
public String getIndAsesor() {
	return IndAsesor;
}
public void setIndAsesor(String indAsesor) {
	IndAsesor = indAsesor;
}
public double getPrimaTotalAnual() {
	return PrimaTotalAnual;
}
public void setPrimaTotalAnual(double primaTotalAnual) {
	PrimaTotalAnual = primaTotalAnual;
}
public double getPrimaTotalFraccionada() {
	return PrimaTotalFraccionada;
}
public void setPrimaTotalFraccionada(double primaTotalFraccionada) {
	PrimaTotalFraccionada = primaTotalFraccionada;
}
public boolean isFlagSeleccionado() {
	return FlagSeleccionado;
}
public void setFlagSeleccionado(boolean flagSeleccionado) {
	FlagSeleccionado = flagSeleccionado;
}
public List<FraccionamientoAsegurado> getFraccionamientoAsegurado() {
	return FraccionamientoAsegurado;
}
public void setFraccionamientoAsegurado(List<FraccionamientoAsegurado> fraccionamientoAsegurado) {
	FraccionamientoAsegurado = fraccionamientoAsegurado;
}

}
