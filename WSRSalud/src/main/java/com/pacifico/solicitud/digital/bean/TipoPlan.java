package com.pacifico.solicitud.digital.bean;

public class TipoPlan {
	
	private String CodigoTipoPlan;
	private String Descripcion;
	private Boolean  FlagSugerido;
	
	public String getCodigoTipoPlan() {
		return CodigoTipoPlan;
	}
	public void setCodigoTipoPlan(String codigoTipoPlan) {
		CodigoTipoPlan = codigoTipoPlan;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public Boolean getFlagSugerido() {
		return FlagSugerido;
	}
	public void setFlagSugerido(Boolean flagSugerido) {
		FlagSugerido = flagSugerido;
	}

	
}
