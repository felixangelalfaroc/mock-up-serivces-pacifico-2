package com.pacifico.solicitud.digital.service.imp;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.pacifico.solicitud.digital.bean.*;
import com.pacifico.solicitud.digital.dto.ADNSaludCotizacionDTO;
import com.pacifico.solicitud.digital.dto.AnularSolicitudDTO;
import com.pacifico.solicitud.digital.dto.CotizacionSaludDTO;
import com.pacifico.solicitud.digital.dto.GetTablasMaestrasDTO;
import com.pacifico.solicitud.digital.dto.PerfilamientoSaludDTO;
import com.pacifico.solicitud.digital.dto.ProspectoDTO;
import com.pacifico.solicitud.digital.json.AnularSolicitudRequest;
import com.pacifico.solicitud.digital.json.CotizacionSaludRequest;
import com.pacifico.solicitud.digital.json.PerfilamientoSaludRequest;
import com.pacifico.solicitud.digital.service.SolicitudDigitalService;

@Service("solicitudDigitalService")
public class SolicitudDigitalServiceImp implements SolicitudDigitalService {	
	
	@Override
	public PerfilamientoSaludDTO getPerfilamientoSalud(PerfilamientoSaludRequest dummy) {
		
		PerfilamientoSaludDTO perfilamientoSaludDTO= new PerfilamientoSaludDTO();

		Plan plan = new Plan();
		plan.setIdPlan("1234567");
		plan.setCodigoPlan("2");
		plan.setCodigoProducto("P0001");
		plan.setCodigoPlan("PLAN1");
		plan.setDescripcionPlan("PLAN 1");
		plan.setCodigoRevision("RV1");
		plan.setFlagSugerido(true);
		
		Fraccionamiento fraccionamiento= new Fraccionamiento();
		
		fraccionamiento.setCodPlanFrac("PLANF1");
		fraccionamiento.setDesPlanFrac("PLAN F1");
		fraccionamiento.setVerPlanFrac("VPLAN1");
		fraccionamiento.setIndDebito("VPLAN1");
		fraccionamiento.setIndAsesor("1");
		
		List<Fraccionamiento> listaFraccionamiento= new ArrayList<>();
		listaFraccionamiento.add(fraccionamiento);	
		
		
		List<Plan> listaPlan = new ArrayList<>();
		listaPlan.add(plan);	
		
		plan.setFraccionamiento(fraccionamiento);
		
		TipoPlan tipoPlan = new TipoPlan();
		tipoPlan.setCodigoTipoPlan("TPLAN1");
		tipoPlan.setDescripcion("TPLAN 1");
		tipoPlan.setFlagSugerido(false);

		List<TipoPlan> listaTipoPlan= new ArrayList<>();
		listaTipoPlan.add(tipoPlan);

		perfilamientoSaludDTO.setListaPlan(listaPlan);
		perfilamientoSaludDTO.setListaTipoPlan(listaTipoPlan);
		
		perfilamientoSaludDTO.setResultCode(200);
		perfilamientoSaludDTO.setResultMessage("OK");
		

		return perfilamientoSaludDTO;
	}	
	
	@Override
	public CotizacionSaludDTO getCotizacionSalud(CotizacionSaludRequest cotizacionSaludRequest) {
		CotizacionSaludDTO cotizacionSaludDTO= new CotizacionSaludDTO();
		
		CotizacionPlan cotizacionPlan= new CotizacionPlan();
		cotizacionPlan.setCodigoProducto("P0001");
		cotizacionPlan.setCodigoPlan("PLAN01");
		cotizacionPlan.setDescripcionPlan("PLAN 01");
		cotizacionPlan.setCodigoRevision("RV1");
		
		Fraccionamiento fraccionamiento= new Fraccionamiento();
		List<Fraccionamiento> listaFraccionamiento= new ArrayList<>();
		
		fraccionamiento.setCodPlanFrac("PLANF1");
		fraccionamiento.setDesPlanFrac("PLAN F1");
		fraccionamiento.setVerPlanFrac("VPLAN1");
		fraccionamiento.setIndDebito("VPLAN1");
		fraccionamiento.setIndAsesor("1");
		fraccionamiento.setMontTotCuotaMen(new BigDecimal(2000.00));
		fraccionamiento.setMontTotCuotaAnu(new BigDecimal(2000.00));				
		
		FraccionamientoAsegurado fa= new FraccionamientoAsegurado();
		List<FraccionamientoAsegurado> listaFraccionamientoAsegurado= new ArrayList<>();
		
		fa.setIdFamiliarDispositivo(1);
		fa.setPrimaMensual(new BigDecimal(2000.00));
		fa.setPrimaAnual(new BigDecimal(2000.00));
		fraccionamiento.setFraccionamientoAsegurado(fa);
		
		Observacion obs= new Observacion();
		List<Observacion> listaObservacion= new ArrayList<>();
		obs.setCodObs("OB1");
		obs.setDesObs("OB 1");	
		
		fraccionamiento.setObservacion(obs);
		
		Asegurados asegurados= new Asegurados();
		List<Asegurados> listaAsegurados= new ArrayList<>();
		asegurados.setIdFamiliarDispositivo(1);
		asegurados.setObservacion(obs);
		
		cotizacionPlan.setMonedaCoberturaMaxima(1);
		cotizacionPlan.setCoberturaMaxima(new BigDecimal(2000.00));
		cotizacionPlan.setCoberturaClinicas("COBERTURA CLINICA 1");
		
		PlanGrupoBeneficio planGrupoBeneficio = new PlanGrupoBeneficio();
		List<PlanGrupoBeneficio> listaPlanGrupoBeneficio= new ArrayList<>();
		planGrupoBeneficio.setIdGrupoBeneficio(1);
		planGrupoBeneficio.setDescripcion("GRUPO DE BENEFICIO 1");
		planGrupoBeneficio.setObservacion("OBSERVACION DE BENEF 1");
			
		RedMedica redMedica= new RedMedica();
		List<RedMedica> listaRedMedica= new ArrayList<>();
		redMedica.setNombreRed("Red Medica 1");
		redMedica.setDeducible("Deducible 1");
		redMedica.setGastoCubierto(1000);			
		
		CentroMedico centroMedico = new CentroMedico();
		List<CentroMedico> listaCentroMedico= new ArrayList<>();
		centroMedico.setIdCentroMedico(1);
		centroMedico.setNombre("Centro Medico 1");
	
		redMedica.setCentroMedico(centroMedico);
		planGrupoBeneficio.setRedMedica(redMedica);
		
		cotizacionPlan.setFraccionamiento(fraccionamiento);
		cotizacionPlan.setAsegurados(asegurados);
		cotizacionPlan.setObservacion(obs);
		cotizacionPlan.setPlanGrupoBeneficio(planGrupoBeneficio);
		
		listaFraccionamiento.add(fraccionamiento);
		listaFraccionamientoAsegurado.add(fa);
		listaObservacion.add(obs);
		listaAsegurados.add(asegurados);
		listaPlanGrupoBeneficio.add(planGrupoBeneficio);
		listaRedMedica.add(redMedica);
		listaCentroMedico.add(centroMedico);		
		
		cotizacionSaludDTO.setCotizacionPlan(cotizacionPlan);
		cotizacionSaludDTO.setListaFraccionamiento(listaFraccionamiento);
		cotizacionSaludDTO.setListaFraccionamientoAsegurado(listaFraccionamientoAsegurado);
		cotizacionSaludDTO.setListaObservacion(listaObservacion);
		cotizacionSaludDTO.setListaAsegurados(listaAsegurados);
		cotizacionSaludDTO.setListaPlanGrupoBeneficio(listaPlanGrupoBeneficio);
		cotizacionSaludDTO.setListaRedMedica(listaRedMedica);
		cotizacionSaludDTO.setListaCentroMedico(listaCentroMedico);
		
		cotizacionSaludDTO.setResultCode(200);
		cotizacionSaludDTO.setResultMessage("OK");
		
		return cotizacionSaludDTO;
	}	
		
	@Override
	public GetTablasMaestrasDTO getTablasMaestras() {
		GetTablasMaestrasDTO getTablasMaestrasDTO= new GetTablasMaestrasDTO();
		List<Parametro> listaParametro=new ArrayList<>();
		Parametro param1=new Parametro();
		param1.setIdParametro(1);
		param1.setValorNumerico(1234);
		param1.setValorCadena("Test");
		param1.setDescripcion("Parametro de prueba");
		listaParametro.add(param1);
		
		Parametro param2=new Parametro();
		param2.setIdParametro(1);
		param2.setValorNumerico(1234);
		param2.setValorCadena("Test");
		param2.setDescripcion("Parametro de prueba");
		listaParametro.add(param2);		

		getTablasMaestrasDTO.setParametro(listaParametro);
		
		List<TablaIndice> listaTablaIndice=new ArrayList<>();
		TablaIndice tabIndex1=new TablaIndice();
		tabIndex1.setIdTabla(1);
		tabIndex1.setNombreTabla("TablaTest");
		listaTablaIndice.add(tabIndex1);
		
		TablaIndice tabIndex2=new TablaIndice();
		tabIndex2.setIdTabla(1);
		tabIndex2.setNombreTabla("TablaTest");
		listaTablaIndice.add(tabIndex2);		
		
		getTablasMaestrasDTO.setTablaIndice(listaTablaIndice);
		
		List<TablaTablas> listaTablaTablas=new ArrayList<>();
		TablaTablas tabTables1=new TablaTablas();
		tabTables1.setIdTabla(1);
		tabTables1.setCodigoCampo(1);
		tabTables1.setValorCadena("Test");
		tabTables1.setValorNumerico(1234);
		tabTables1.setValorAuxiliarCadena("TestAuxiliar");
		tabTables1.setEquivalenciaPPS("TestEquivPPS");
		tabTables1.setEquivalenciaVIAP("TestEquivVIAP");
		listaTablaTablas.add(tabTables1);
		
		TablaTablas tabTables2=new TablaTablas();		
		tabTables2.setIdTabla(1);
		tabTables2.setCodigoCampo(1);
		tabTables2.setValorCadena("Test");
		tabTables2.setValorNumerico(1234);
		tabTables2.setValorAuxiliarCadena("TestAuxiliar");
		tabTables2.setEquivalenciaPPS("TestEquivPPS");
		tabTables2.setEquivalenciaVIAP("TestEquivVIAP");
		listaTablaTablas.add(tabTables2);
		
		getTablasMaestrasDTO.setTablaTablas(listaTablaTablas);
		
		List<Parentesco> listaParentesco = new ArrayList<>();
		Parentesco paren1=new Parentesco();
		paren1.setCodigoParentesco(1);
		paren1.setDescripcion("Tester");
		paren1.setCodigoSexo(1);
		paren1.setCodigoParentescoPPS("1");
		listaParentesco.add(paren1);
		
		Parentesco paren2=new Parentesco();
		paren2.setCodigoParentesco(1);
		paren2.setDescripcion("Tester");
		paren2.setCodigoSexo(1);
		paren2.setCodigoParentescoPPS("1");	
		listaParentesco.add(paren2);
		
		getTablasMaestrasDTO.setParentesco(listaParentesco);
		
		List<Pais> listaPais=new ArrayList<>();
		Pais pais1=new Pais();
		pais1.setCodigoPais(1);
		pais1.setDescripcion("Testland");
		pais1.setNacionalidad("Tester");
		listaPais.add(pais1);
		
		Pais pais2=new Pais();
		pais2.setCodigoPais(1);
		pais2.setDescripcion("Testland");
		pais2.setNacionalidad("Tester");
		listaPais.add(pais2);
		
		getTablasMaestrasDTO.setPais(listaPais);
		
		List<Departamento> listaDepartamento = new ArrayList<>();
		Departamento depa1=new Departamento();
		depa1.setCodigoDepartamento(1);
		depa1.setDescripcion("Test");
		depa1.setPrefijoTelefonico("+1");
		listaDepartamento.add(depa1);
		
		Departamento depa2=new Departamento();
		depa2.setCodigoDepartamento(1);
		depa2.setDescripcion("Test");
		depa2.setPrefijoTelefonico("+1");		
		listaDepartamento.add(depa2);
		
		getTablasMaestrasDTO.setDepartamento(listaDepartamento);
		
		List<Provincia> listaProvincia = new ArrayList<>();
		Provincia prov1=new Provincia();
		prov1.setCodigoProvincia(1);
		prov1.setCodigoDepartamento(1);
		prov1.setDescripcion("Test");
		listaProvincia.add(prov1);
		
		Provincia prov2=new Provincia();
		prov2.setCodigoProvincia(1);
		prov2.setCodigoDepartamento(1);
		prov2.setDescripcion("Test");
		listaProvincia.add(prov2);
		
		getTablasMaestrasDTO.setProvincia(listaProvincia);
		
		List<Distrito> listaDistrito=new ArrayList<>();
		Distrito dist1=new Distrito();
		dist1.setCodigoDepartamento(1);
		dist1.setCodigoDistrito(1);
		dist1.setCodigoProvincia(1);
		dist1.setDescripcion("Test");
		listaDistrito.add(dist1);
		
		Distrito dist2=new Distrito();
		dist2.setCodigoDepartamento(1);
		dist2.setCodigoDistrito(1);
		dist2.setCodigoProvincia(1);
		dist2.setDescripcion("Test");
		listaDistrito.add(dist2);
		
		getTablasMaestrasDTO.setDistrito(listaDistrito);
		
		List<TipoDescuento> listaTipoDescuento=new ArrayList<>();
		TipoDescuento tipDesc1=new TipoDescuento();
		tipDesc1.setCodigoDescuentoPPS("1");
		tipDesc1.setFlagAdjuntarFotoCheck(true);
		tipDesc1.setIdTipoDescuento(1);
		tipDesc1.setDescripcion("Test");
		listaTipoDescuento.add(tipDesc1);
		
		TipoDescuento tipDesc2=new TipoDescuento();
		tipDesc2.setCodigoDescuentoPPS("1");
		tipDesc2.setFlagAdjuntarFotoCheck(true);
		tipDesc2.setIdTipoDescuento(1);
		tipDesc2.setDescripcion("Test");
		listaTipoDescuento.add(tipDesc2);
		
		getTablasMaestrasDTO.setTipoDescuento(listaTipoDescuento);
		
		List<Plan> listaPlan=new ArrayList<>();
		Plan plan1=new Plan();
		plan1.setIdPlan("1");
		plan1.setCodigoPlan("1");
		plan1.setCodigoProducto("Test");
		plan1.setCodigoProducto("1");
		plan1.setNombreProducto("TestProduct");
		plan1.setDescripcionPlan("Test");
		plan1.setCodigoPlan("1");
		plan1.setCodigoRevision("1");
		plan1.setRangoEdadInicio(1);
		plan1.setRangoEdadFin(99);
		plan1.setFlagPlanBase(true);
		plan1.setAplicaRecargoFumador(true);
		List<PlanParentesco> listaPlanParentesco=new ArrayList<>();
		PlanParentesco planParen1=new PlanParentesco();
		planParen1.setCodigoParentesco(1);
		listaPlanParentesco.add(planParen1);
		plan1.setPlanParentesco(listaPlanParentesco);
		listaPlan.add(plan1);
		
		Plan plan2=new Plan();
		plan2.setIdPlan("1");
		plan2.setCodigoPlan("1");
		plan2.setCodigoProducto("Test");
		plan2.setCodigoProducto("1");
		plan2.setNombreProducto("TestProduct");
		plan2.setDescripcionPlan("Test");
		plan2.setCodigoPlan("1");
		plan2.setCodigoRevision("1");
		plan2.setRangoEdadInicio(1);
		plan2.setRangoEdadFin(99);
		plan2.setFlagPlanBase(true);
		plan2.setAplicaRecargoFumador(true);
		PlanParentesco planParen2=new PlanParentesco();
		planParen2.setCodigoParentesco(1);
		listaPlanParentesco.add(planParen2);
		plan2.setPlanParentesco(listaPlanParentesco);
		listaPlan.add(plan2);		
		
		getTablasMaestrasDTO.setPlan(listaPlan);
		
		List<TipoCobranza> listaTipoCobranza=new ArrayList<>();
		TipoCobranza tipCobr1=new TipoCobranza();
		tipCobr1.setCodigoTipoCobranza(1);
		tipCobr1.setDescripcion("Test");
		tipCobr1.setAplicaPagoRecurrente(true);
		tipCobr1.setAplicaPrimerPago(true);
		listaTipoCobranza.add(tipCobr1);
		
		TipoCobranza tipCobr2=new TipoCobranza();
		tipCobr2.setCodigoTipoCobranza(1);
		tipCobr2.setDescripcion("Test");
		tipCobr2.setAplicaPagoRecurrente(true);
		tipCobr2.setAplicaPrimerPago(true);
		listaTipoCobranza.add(tipCobr2);	
		
		getTablasMaestrasDTO.setTipoCobranza(listaTipoCobranza);
		
		List<Entidad> listaEntidad= new ArrayList<>();
		Entidad ent1=new Entidad();
		ent1.setCodigoEntidad(1);
		ent1.setCodigoTipoCobranza(1);
		ent1.setDescripcion("Test");
		listaEntidad.add(ent1);
		
		Entidad ent2=new Entidad();
		ent2.setCodigoEntidad(1);
		ent2.setCodigoTipoCobranza(1);
		ent2.setDescripcion("Test");
		listaEntidad.add(ent2);
		
		getTablasMaestrasDTO.setEntidad(listaEntidad);
		
		List<TipoCuentaEntidad> listaTipoCuentaEntidad=new ArrayList<>();
		TipoCuentaEntidad tipCuenEnt1=new TipoCuentaEntidad();
		tipCuenEnt1.setCodigoEntidad(1);
		tipCuenEnt1.setCodigoTipoCuenta(1);
		tipCuenEnt1.setCodigoMoneda(1);
		tipCuenEnt1.setPlantillNumeroCuenta("Test");
		listaTipoCuentaEntidad.add(tipCuenEnt1);
		
		TipoCuentaEntidad tipCuenEnt2=new TipoCuentaEntidad();
		tipCuenEnt2.setCodigoEntidad(1);
		tipCuenEnt2.setCodigoTipoCuenta(1);
		tipCuenEnt2.setCodigoMoneda(1);
		tipCuenEnt2.setPlantillNumeroCuenta("Test");
		listaTipoCuentaEntidad.add(tipCuenEnt2);		
		
		getTablasMaestrasDTO.setTipoCuentaEntidad(listaTipoCuentaEntidad);
		
		List<CuentaEntidadValidacion> listaCuentaEntidadValidacion = new ArrayList<>();
		CuentaEntidadValidacion cuenEntVal1=new CuentaEntidadValidacion();
		cuenEntVal1.setCodigoValidacion(1);
		cuenEntVal1.setCodigoEntidad(1);
		cuenEntVal1.setCodigoTipoCuenta(1);
		cuenEntVal1.setPosicionInicial(1);
		cuenEntVal1.setPosicionFinal(1);
		cuenEntVal1.setCaracterValidador("T");
		cuenEntVal1.setDescripcion("Test");
		listaCuentaEntidadValidacion.add(cuenEntVal1);
		
		CuentaEntidadValidacion cuenEntVal2=new CuentaEntidadValidacion();
		cuenEntVal2.setCodigoValidacion(1);
		cuenEntVal2.setCodigoEntidad(1);
		cuenEntVal2.setCodigoTipoCuenta(1);
		cuenEntVal2.setPosicionInicial(1);
		cuenEntVal2.setPosicionFinal(1);
		cuenEntVal2.setCaracterValidador("T");
		cuenEntVal2.setDescripcion("Test");
		listaCuentaEntidadValidacion.add(cuenEntVal2);		
		
		getTablasMaestrasDTO.setCuentaEntidadValidacion(listaCuentaEntidadValidacion);
		
		List<TarjetaNoDeseada> listaTarjetaNoDeseada=new ArrayList<>();
		TarjetaNoDeseada tarj1=new TarjetaNoDeseada();
		tarj1.setCodigoTarjetaNoDeseada(1);
		tarj1.setNombreEntidad("TestBank");
		tarj1.setBin("1234 56");
		listaTarjetaNoDeseada.add(tarj1);
		
		TarjetaNoDeseada tarj2=new TarjetaNoDeseada();
		tarj2.setCodigoTarjetaNoDeseada(1);
		tarj2.setNombreEntidad("TestBank");
		tarj2.setBin("1234 56");
		listaTarjetaNoDeseada.add(tarj2);

		getTablasMaestrasDTO.setTarjetaNoDeseada(listaTarjetaNoDeseada);
		
		List<BancoPreguntaSalud> listaBancoPreguntaSalud=new ArrayList<>();
		BancoPreguntaSalud preg1=new BancoPreguntaSalud();
		preg1.setIdBancoPreguntaSalud(1);
		preg1.setCodigoProducto("1");
		preg1.setDescripcionPregunta("TestQuestion");
		preg1.setAplicaMasculino(true);
		preg1.setAplicaFemenino(true);
		preg1.setTipoRespuesta(1);
		listaBancoPreguntaSalud.add(preg1);
		
		BancoPreguntaSalud preg2=new BancoPreguntaSalud();
		preg2.setIdBancoPreguntaSalud(1);
		preg2.setCodigoProducto("1");
		preg2.setDescripcionPregunta("TestQuestion");
		preg2.setAplicaMasculino(true);
		preg2.setAplicaFemenino(true);
		preg2.setTipoRespuesta(1);
		listaBancoPreguntaSalud.add(preg2);
		
		getTablasMaestrasDTO.setBancoPreguntaSalud(listaBancoPreguntaSalud);
		
		getTablasMaestrasDTO.setResultCode(200);
		getTablasMaestrasDTO.setResultMessage("OK");
		return getTablasMaestrasDTO;
	}

	@Override
	public AnularSolicitudDTO setSolicitudMultiple(AnularSolicitudRequest anularSolicitudRequest) {
		
		AnularSolicitudDTO anularSolicitudDTO = new AnularSolicitudDTO();
		SolicitudMultiple solicitudMultiple = new SolicitudMultiple();
		
		solicitudMultiple.setIdSolicitudMultiple(1);
		solicitudMultiple.setCodigoEstado(1);
		
		anularSolicitudDTO.setSolicitudMultiple(solicitudMultiple);
		
		anularSolicitudDTO.setResultCode(200);
		anularSolicitudDTO.setResultMessage("OK");
		
		return anularSolicitudDTO;
	}

	@Override
	public ProspectoDTO getProspecto() {
		ProspectoDTO prospectoDTO = new ProspectoDTO();
		Prospecto prospecto= new Prospecto();
		prospecto.setIdProspecto(1);
		prospecto.setNombreCompleto("Tester Testing Test");
		prospecto.setNombres("Tester");
		prospecto.setApellidoPaterno("Testing");
		prospecto.setApellidoMaterno("Test");
		prospecto.setTipoProspecto(1);
		
		CotizacionSolicitud cotizacionSolicitud=new CotizacionSolicitud();
		cotizacionSolicitud.setIdCotizacion(1);
		cotizacionSolicitud.setIdSolicitudMultiple(1);
		cotizacionSolicitud.setCodigoEstadoSolicitudMultiple(1);
		
		List<CotizacionSolicitud> listaCotizacionSolicitud = new ArrayList<>();
		listaCotizacionSolicitud.add(cotizacionSolicitud);	
		
		List<Prospecto> listaProspectos= new ArrayList<>();
		listaProspectos.add(prospecto);	
		
		prospecto.setCotizacionSolicitud(cotizacionSolicitud);
		
		prospectoDTO.setListaProspecto(listaProspectos);
		prospectoDTO.setResultCode(200);
		prospectoDTO.setResultMessage("OK");
		return prospectoDTO;
	}
	
	@Override
	public ADNSaludCotizacionDTO getAdnSolicitudCotizacion() {
		ADNSaludCotizacionDTO adnSaludCotizacionDTO = new ADNSaludCotizacionDTO();
		ProspectoADN prospectoADN= new ProspectoADN();
		prospectoADN.setIdProspecto(1);
		prospectoADN.setNombres("Tester");
		prospectoADN.setApellidoPaterno("Testing");
		prospectoADN.setApellidoMaterno("Test");
		prospectoADN.setCodigoEstadoCivil(1);
		prospectoADN.setCodigoTipoDocumento(1);
		prospectoADN.setNumeroDocumento("87654321");
		prospectoADN.setTelefonoCelular("987654321");
		prospectoADN.setCorreoElectronico1("test@test.test");
		prospectoADN.setCodigoNacionalidad(1);
	
		
		List<Familiar> listaFamiliar = new ArrayList<>();
		Familiar familiar = new Familiar();
		familiar.setNombres("Tester");
		familiar.setApellidoPaterno("Testing");
		familiar.setApellidoMaterno("Test");
		familiar.setIdFamiliarDispositivo(1);
		listaFamiliar.add(familiar);
		
		Cotizacion cotizacion=new Cotizacion();
		cotizacion.setIdCotizacion(1);
		cotizacion.setFechaCotizacion("01/01/2000");
		List<CotizacionPlan> listaCotizacionPlan = new ArrayList<>();
		CotizacionPlan cotizacionPlan = new CotizacionPlan();
		cotizacionPlan.setIdCotizacionPlan(1);
		cotizacionPlan.setIdPlan(1);
		cotizacionPlan.setDescripcion("Test");
		cotizacionPlan.setCodigoPlan("T");
		cotizacionPlan.setCodigoRevision("T");
		cotizacionPlan.setCodigoProducto("T");
		cotizacionPlan.setIdTipoDescuento(1);
		listaCotizacionPlan.add(cotizacionPlan);
		
		List<CotizacionAsegurado> listaCotizacionAsegurado= new ArrayList<>();
		CotizacionAsegurado cotizacionAsegurado= new CotizacionAsegurado();
		cotizacionAsegurado.setCodigoTipoFamiliar(1);
		cotizacionAsegurado.setIdFamiliarDispositivo(1);
		cotizacionAsegurado.setFechaNacimiento("01/01/2000");
		cotizacionAsegurado.setCondicionFumador(true);
		cotizacionAsegurado.setTalla(1.80);
		cotizacionAsegurado.setPeso(85);
		cotizacionAsegurado.setFlagTitular(true);
		cotizacionAsegurado.setCodigoSexo(1);
		
		listaCotizacionAsegurado.add(cotizacionAsegurado);		
		cotizacionPlan.setCotizacionAsegurado(listaCotizacionAsegurado);
		List<CotizacionFraccionamiento> listaCotizacionFraccionamiento=new ArrayList<>();
		CotizacionFraccionamiento cotizacionFraccionamiento =new CotizacionFraccionamiento();
		cotizacionFraccionamiento.setCodPlanFrac("1");
		cotizacionFraccionamiento.setDesPlanFrac("Test");
		cotizacionFraccionamiento.setVerPlanFrac("T");
		cotizacionFraccionamiento.setIndDebito("T");
		cotizacionFraccionamiento.setIndAsesor("T");
		cotizacionFraccionamiento.setPrimaTotalAnual(999.99);
		cotizacionFraccionamiento.setPrimaTotalFraccionada(999.99);
		cotizacionFraccionamiento.setFlagSeleccionado(true);
		
		List<FraccionamientoAsegurado> listaFraccionamientoAsegurado= new ArrayList<>();
		FraccionamientoAsegurado fraccionamientoAsegurado= new FraccionamientoAsegurado();
		fraccionamientoAsegurado.setIdFamiliarDispositivo(1);
		fraccionamientoAsegurado.setPrimaAnual(new BigDecimal("10364055.81"));
		fraccionamientoAsegurado.setPrimaMensual(new BigDecimal("10364055.81"));
		listaFraccionamientoAsegurado.add(fraccionamientoAsegurado);
		
		cotizacionFraccionamiento.setFraccionamientoAsegurado(listaFraccionamientoAsegurado);
		listaCotizacionFraccionamiento.add(cotizacionFraccionamiento);
		
		cotizacionPlan.setCotizacionFraccionamiento(listaCotizacionFraccionamiento);
		cotizacion.setCotizacionPlan(listaCotizacionPlan);
		
		adnSaludCotizacionDTO.setProspectoADN(prospectoADN);		
		adnSaludCotizacionDTO.setFamiliar(listaFamiliar);
		adnSaludCotizacionDTO.setCotizacion(cotizacion);		
		adnSaludCotizacionDTO.setResultCode(200);
		adnSaludCotizacionDTO.setResultMessage("OK");
		
		return adnSaludCotizacionDTO;
	}		
	
	


}
