package com.pacifico.solicitud.digital.bean;

public class TablaTablas {
private int idTabla;
private int CodigoCampo;
private String ValorCadena;
private double ValorNumerico;
private String ValorAuxiliarCadena;
private String EquivalenciaVIAP;
private String EquivalenciaPPS;
public int getIdTabla() {
	return idTabla;
}
public void setIdTabla(int idTabla) {
	this.idTabla = idTabla;
}
public int getCodigoCampo() {
	return CodigoCampo;
}
public void setCodigoCampo(int codigoCampo) {
	CodigoCampo = codigoCampo;
}
public String getValorCadena() {
	return ValorCadena;
}
public void setValorCadena(String valorCadena) {
	ValorCadena = valorCadena;
}
public double getValorNumerico() {
	return ValorNumerico;
}
public void setValorNumerico(double valorNumerico) {
	ValorNumerico = valorNumerico;
}
public String getValorAuxiliarCadena() {
	return ValorAuxiliarCadena;
}
public void setValorAuxiliarCadena(String valorAuxiliarCadena) {
	ValorAuxiliarCadena = valorAuxiliarCadena;
}
public String getEquivalenciaVIAP() {
	return EquivalenciaVIAP;
}
public void setEquivalenciaVIAP(String equivalenciaVIAP) {
	EquivalenciaVIAP = equivalenciaVIAP;
}
public String getEquivalenciaPPS() {
	return EquivalenciaPPS;
}
public void setEquivalenciaPPS(String equivalenciaPPS) {
	EquivalenciaPPS = equivalenciaPPS;
}

}
