package com.pacifico.solicitud.digital.util;

public class GenericConstants {
	// Codigos y mensajes Genericos
	public static final String F99_CODE = "F99";
	public static final String F99_MSG = "Error General";
	public static final String S00_CODE = "S00";
	public static final String S00_MSG = "Ok";
	public static final String NAMESPACE_MAL_FORMADO = "Namespace mal formado";
	
	public static final String E10_CODE = "E10";
	public static final String E10_MSG = "Codigo de fase, codigo de producto, items son valores requeridos.";
	public static final String E11_CODE = "E11";
	public static final String E11_MSG = "Error validando respuesta";
	public static final String E12_CODE = "E12";
	public static final String E12_MSG = "Valor incorrecto para: ";
	public static final String E13_CODE = "E13";
	public static final String E13_MSG = "Error de Negocio";

	// Consume SOAP
	public static final String EW01_CODE = "EW01";
	public static final String EW01_MSG = "No se tiene respuesta del servicio: ";
	public static final String EW02_CODE = "E02";
	public static final String EW02_MSG = "Fase es requerido.";
	public static final String EW03_CODE = "E03";
	public static final String EW03_MSG = "Producto es requerido.";
	public static final String EW04_CODE = "E04";
	public static final String EW04_MSG = "Datos incompletos";

	public static final String SEPARADOR_CODE_ERROR = " - ";
	
	private GenericConstants() {
	}
}