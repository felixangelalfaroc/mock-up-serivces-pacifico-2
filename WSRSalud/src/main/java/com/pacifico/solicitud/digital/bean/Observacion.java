package com.pacifico.solicitud.digital.bean;

public class Observacion {

 private String CodObs;
 private String desObs;
 
public String getCodObs() {
	return CodObs;
}
public void setCodObs(String codObs) {
	CodObs = codObs;
}
public String getDesObs() {
	return desObs;
}
public void setDesObs(String desObs) {
	this.desObs = desObs;
}

	
}
