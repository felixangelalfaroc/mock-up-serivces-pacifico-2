package com.pacifico.solicitud.digital.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pacifico.solicitud.digital.bean.SolicitudMultiple;

public class AnularSolicitudDTO {
	@JsonProperty("SolicitudMultiple")
	private SolicitudMultiple solicitudMultiple;
	
	private int ResultCode;
	private String ResultMessage;

	public SolicitudMultiple getSolicitudMultiple() {
		return solicitudMultiple;
	}

	public void setSolicitudMultiple(SolicitudMultiple solicitudMultiple) {
		this.solicitudMultiple = solicitudMultiple;
	}

	public int getResultCode() {
		return ResultCode;
	}

	public void setResultCode(int resultCode) {
		ResultCode = resultCode;
	}

	public String getResultMessage() {
		return ResultMessage;
	}

	public void setResultMessage(String resultMessage) {
		ResultMessage = resultMessage;
	}	
	
}
