package com.pacifico.solicitud.digital.bean;

public class Familiar {
private String Nombres;
private String ApellidoPaterno;
private String ApellidoMaterno;
private int IdFamiliarDispositivo;
public String getNombres() {
	return Nombres;
}
public void setNombres(String nombres) {
	Nombres = nombres;
}
public String getApellidoPaterno() {
	return ApellidoPaterno;
}
public void setApellidoPaterno(String apellidoPaterno) {
	ApellidoPaterno = apellidoPaterno;
}
public String getApellidoMaterno() {
	return ApellidoMaterno;
}
public void setApellidoMaterno(String apellidoMaterno) {
	ApellidoMaterno = apellidoMaterno;
}
public int getIdFamiliarDispositivo() {
	return IdFamiliarDispositivo;
}
public void setIdFamiliarDispositivo(int idFamiliarDispositivo) {
	IdFamiliarDispositivo = idFamiliarDispositivo;
}

}
