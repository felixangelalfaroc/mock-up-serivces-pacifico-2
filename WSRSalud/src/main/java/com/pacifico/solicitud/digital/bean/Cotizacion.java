package com.pacifico.solicitud.digital.bean;

import java.util.List;

public class Cotizacion {
private int IdCotizacion;
private String FechaCotizacion;
private List<CotizacionPlan> CotizacionPlan;
public int getIdCotizacion() {
	return IdCotizacion;
}
public void setIdCotizacion(int idCotizacion) {
	IdCotizacion = idCotizacion;
}
public String getFechaCotizacion() {
	return FechaCotizacion;
}
public void setFechaCotizacion(String fechaCotizacion) {
	FechaCotizacion = fechaCotizacion;
}
public List<CotizacionPlan> getCotizacionPlan() {
	return CotizacionPlan;
}
public void setCotizacionPlan(List<CotizacionPlan> cotizacionPlan) {
	CotizacionPlan = cotizacionPlan;
}

}
