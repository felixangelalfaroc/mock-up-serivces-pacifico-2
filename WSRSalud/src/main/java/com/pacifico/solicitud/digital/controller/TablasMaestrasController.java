package com.pacifico.solicitud.digital.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pacifico.solicitud.digital.dto.GetTablasMaestrasDTO;
import com.pacifico.solicitud.digital.service.SolicitudDigitalService;

@RestController
@RequestMapping("tablasMaestras")
public class TablasMaestrasController {
	
	@Autowired
	private SolicitudDigitalService solicitudDigitalService;

	@RequestMapping(method = RequestMethod.GET)
	public GetTablasMaestrasDTO obtenerTablasMaestras() {

		GetTablasMaestrasDTO dummy = new GetTablasMaestrasDTO();
		dummy = solicitudDigitalService.getTablasMaestras();
		
		return dummy;

	}

}
