package com.pacifico.solicitud.digital.controller;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pacifico.solicitud.digital.dto.CotizacionSaludDTO;
import com.pacifico.solicitud.digital.json.CotizacionSaludRequest;
import com.pacifico.solicitud.digital.service.SolicitudDigitalService;

	@RestController
	@RequestMapping("cotizacionSalud")
	public class CotizacionSaludController {

		@Autowired
		private SolicitudDigitalService solicitudDigitalService;

		@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
		public CotizacionSaludDTO obtenerCotizacionSalud(@RequestBody CotizacionSaludRequest cotizacionSaludRequest) {
			
		CotizacionSaludDTO cotizacionSaludDTO = new CotizacionSaludDTO();
		cotizacionSaludDTO = solicitudDigitalService.getCotizacionSalud(cotizacionSaludRequest);
			
			return cotizacionSaludDTO;
		}

	}
