package com.pacifico.solicitud.digital.bean;

public class RedMedica {

	private String nombreRed;
	private String deducible;
	private Integer gastoCubierto;
	private CentroMedico centroMedico;
	
	public String getNombreRed() {
		return nombreRed;
	}
	public void setNombreRed(String nombreRed) {
		this.nombreRed = nombreRed;
	}
	public String getDeducible() {
		return deducible;
	}
	public void setDeducible(String deducible) {
		this.deducible = deducible;
	}
	public Integer getGastoCubierto() {
		return gastoCubierto;
	}
	public void setGastoCubierto(Integer gastoCubierto) {
		this.gastoCubierto = gastoCubierto;
	}
	public CentroMedico getCentroMedico() {
		return centroMedico;
	}
	public void setCentroMedico(CentroMedico centroMedico) {
		this.centroMedico = centroMedico;
	}
	
}
