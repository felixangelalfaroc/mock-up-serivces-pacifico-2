package com.pacifico.solicitud.digital.bean;

import java.util.List;

public class Plan {
	private String	IdPlan;
	private String	CodigoTipoPlan;
	private String	CodigoProducto;
	private String	CodigoPlan;
	private String	DescripcionPlan;
	private String	CodigoRevision;
	private Boolean	FlagSugerido;
	private Fraccionamiento fraccionamiento;
	
	private int RangoEdadInicio;
	private int RangoEdadFin;
	private boolean FlagPlanBase;
	private boolean AplicaRecargoFumador;
	private List<PlanParentesco> PlanParentesco;
	private String NombreProducto;
	
	public String getNombreProducto() {
		return NombreProducto;
	}
	public void setNombreProducto(String nombreProducto) {
		NombreProducto = nombreProducto;
	}
	public String getIdPlan() {
		return IdPlan;
	}
	public void setIdPlan(String idPlan) {
		IdPlan = idPlan;
	}
	public String getCodigoTipoPlan() {
		return CodigoTipoPlan;
	}
	public void setCodigoTipoPlan(String codigoTipoPlan) {
		CodigoTipoPlan = codigoTipoPlan;
	}
	public String getCodigoProducto() {
		return CodigoProducto;
	}
	public void setCodigoProducto(String codigoProducto) {
		CodigoProducto = codigoProducto;
	}
	public String getCodigoPlan() {
		return CodigoPlan;
	}
	public void setCodigoPlan(String codigoPlan) {
		CodigoPlan = codigoPlan;
	}
	public String getDescripcionPlan() {
		return DescripcionPlan;
	}
	public void setDescripcionPlan(String descripcionPlan) {
		DescripcionPlan = descripcionPlan;
	}
	public String getCodigoRevision() {
		return CodigoRevision;
	}
	public void setCodigoRevision(String codigoRevision) {
		CodigoRevision = codigoRevision;
	}

	public Boolean getFlagSugerido() {
		return FlagSugerido;
	}
	public void setFlagSugerido(Boolean flagSugerido) {
		FlagSugerido = flagSugerido;
	}
	public Fraccionamiento getFraccionamiento() {
		return fraccionamiento;
	}
	public void setFraccionamiento(Fraccionamiento fraccionamiento) {
		this.fraccionamiento = fraccionamiento;
	}
	public int getRangoEdadInicio() {
		return RangoEdadInicio;
	}
	public void setRangoEdadInicio(int rangoEdadInicio) {
		RangoEdadInicio = rangoEdadInicio;
	}
	public int getRangoEdadFin() {
		return RangoEdadFin;
	}
	public void setRangoEdadFin(int rangoEdadFin) {
		RangoEdadFin = rangoEdadFin;
	}
	public boolean isFlagPlanBase() {
		return FlagPlanBase;
	}
	public void setFlagPlanBase(boolean flagPlanBase) {
		FlagPlanBase = flagPlanBase;
	}
	public boolean isAplicaRecargoFumador() {
		return AplicaRecargoFumador;
	}
	public void setAplicaRecargoFumador(boolean aplicaRecargoFumador) {
		AplicaRecargoFumador = aplicaRecargoFumador;
	}
	public List<PlanParentesco> getPlanParentesco() {
		return PlanParentesco;
	}
	public void setPlanParentesco(List<PlanParentesco> planParentesco) {
		PlanParentesco = planParentesco;
	}
	
	
}
