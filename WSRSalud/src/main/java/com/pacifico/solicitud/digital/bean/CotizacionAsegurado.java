package com.pacifico.solicitud.digital.bean;

public class CotizacionAsegurado {
private int CodigoTipoFamiliar;
private int IdFamiliarDispositivo;
private String FechaNacimiento;
private boolean CondicionFumador;
private double Talla;
private double Peso;
private boolean FlagTitular;
private int CodigoSexo;
public int getCodigoTipoFamiliar() {
	return CodigoTipoFamiliar;
}
public void setCodigoTipoFamiliar(int codigoTipoFamiliar) {
	CodigoTipoFamiliar = codigoTipoFamiliar;
}
public int getIdFamiliarDispositivo() {
	return IdFamiliarDispositivo;
}
public void setIdFamiliarDispositivo(int idFamiliarDispositivo) {
	IdFamiliarDispositivo = idFamiliarDispositivo;
}
public String getFechaNacimiento() {
	return FechaNacimiento;
}
public void setFechaNacimiento(String fechaNacimiento) {
	FechaNacimiento = fechaNacimiento;
}
public boolean isCondicionFumador() {
	return CondicionFumador;
}
public void setCondicionFumador(boolean condicionFumador) {
	CondicionFumador = condicionFumador;
}
public double getTalla() {
	return Talla;
}
public void setTalla(double talla) {
	Talla = talla;
}
public double getPeso() {
	return Peso;
}
public void setPeso(double peso) {
	Peso = peso;
}
public boolean isFlagTitular() {
	return FlagTitular;
}
public void setFlagTitular(boolean flagTitular) {
	FlagTitular = flagTitular;
}
public int getCodigoSexo() {
	return CodigoSexo;
}
public void setCodigoSexo(int codigoSexo) {
	CodigoSexo = codigoSexo;
}

}
