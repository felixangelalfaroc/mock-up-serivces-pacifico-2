package com.pacifico.solicitud.digital.dto;

import java.util.List;

import com.pacifico.solicitud.digital.bean.*;

public class ADNSaludCotizacionDTO {
	private ProspectoADN ProspectoADN;
	private List<Familiar> Familiar;
	private Cotizacion Cotizacion;
	private int ResultCode;
	private String ResultMessage;

	public ProspectoADN getProspectoADN() {
		return ProspectoADN;
	}

	public void setProspectoADN(ProspectoADN prospectoADN) {
		ProspectoADN = prospectoADN;
	}

	public List<Familiar> getFamiliar() {
		return Familiar;
	}

	public void setFamiliar(List<Familiar> familiar) {
		Familiar = familiar;
	}

	public Cotizacion getCotizacion() {
		return Cotizacion;
	}

	public void setCotizacion(Cotizacion cotizacion) {
		Cotizacion = cotizacion;
	}

	public int getResultCode() {
		return ResultCode;
	}

	public void setResultCode(int resultCode) {
		ResultCode = resultCode;
	}

	public String getResultMessage() {
		return ResultMessage;
	}

	public void setResultMessage(String resultMessage) {
		ResultMessage = resultMessage;
	}

}
