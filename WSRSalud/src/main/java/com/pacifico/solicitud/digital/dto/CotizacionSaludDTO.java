package com.pacifico.solicitud.digital.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pacifico.solicitud.digital.bean.Asegurados;
import com.pacifico.solicitud.digital.bean.CentroMedico;
import com.pacifico.solicitud.digital.bean.CotizacionPlan;
import com.pacifico.solicitud.digital.bean.Fraccionamiento;
import com.pacifico.solicitud.digital.bean.FraccionamientoAsegurado;
import com.pacifico.solicitud.digital.bean.Observacion;
import com.pacifico.solicitud.digital.bean.PlanGrupoBeneficio;
import com.pacifico.solicitud.digital.bean.RedMedica;

public class CotizacionSaludDTO {
	@JsonProperty("CotizacionPlan")
	private CotizacionPlan cotizacionPlan;
    @JsonProperty("Fraccionamiento")
	private List<Fraccionamiento>listaFraccionamiento;
    @JsonProperty("FraccionamientoAsegurado")
	private List<FraccionamientoAsegurado>listaFraccionamientoAsegurado;
    @JsonProperty("Observacion")
	private List<Observacion> listaObservacion;
    @JsonProperty("Asegurado")
	private List<Asegurados> listaAsegurados;
    @JsonProperty("PlanGrupoBeneficio")
	private List<PlanGrupoBeneficio> listaPlanGrupoBeneficio;
    @JsonProperty("RedMedica")
	private List<RedMedica> listaRedMedica;
    @JsonProperty("CentroMedico")
	private List<CentroMedico> listaCentroMedico;
    private int ResultCode;
    private String ResultMessage;
    
	public CotizacionPlan getCotizacionPlan() {
		return cotizacionPlan;
	}
	public void setCotizacionPlan(CotizacionPlan cotizacionPlan) {
		this.cotizacionPlan = cotizacionPlan;
	}
	public List<Fraccionamiento> getListaFraccionamiento() {
		return listaFraccionamiento;
	}
	public void setListaFraccionamiento(List<Fraccionamiento> listaFraccionamiento) {
		this.listaFraccionamiento = listaFraccionamiento;
	}
	public List<FraccionamientoAsegurado> getListaFraccionamientoAsegurado() {
		return listaFraccionamientoAsegurado;
	}
	public void setListaFraccionamientoAsegurado(
			List<FraccionamientoAsegurado> listaFraccionamientoAsegurado) {
		this.listaFraccionamientoAsegurado = listaFraccionamientoAsegurado;
	}
	public List<Observacion> getListaObservacion() {
		return listaObservacion;
	}
	public void setListaObservacion(List<Observacion> listaObservacion) {
		this.listaObservacion = listaObservacion;
	}
	public List<Asegurados> getListaAsegurados() {
		return listaAsegurados;
	}
	public void setListaAsegurados(List<Asegurados> listaAsegurados) {
		this.listaAsegurados = listaAsegurados;
	}
	public List<PlanGrupoBeneficio> getListaPlanGrupoBeneficio() {
		return listaPlanGrupoBeneficio;
	}
	public void setListaPlanGrupoBeneficio(
			List<PlanGrupoBeneficio> listaPlanGrupoBeneficio) {
		this.listaPlanGrupoBeneficio = listaPlanGrupoBeneficio;
	}
	public List<RedMedica> getListaRedMedica() {
		return listaRedMedica;
	}
	public void setListaRedMedica(List<RedMedica> listaRedMedica) {
		this.listaRedMedica = listaRedMedica;
	}
	public List<CentroMedico> getListaCentroMedico() {
		return listaCentroMedico;
	}
	public void setListaCentroMedico(List<CentroMedico> listaCentroMedico) {
		this.listaCentroMedico = listaCentroMedico;
	}
	public int getResultCode() {
		return ResultCode;
	}
	public void setResultCode(int resultCode) {
		ResultCode = resultCode;
	}
	public String getResultMessage() {
		return ResultMessage;
	}
	public void setResultMessage(String resultMessage) {
		ResultMessage = resultMessage;
	}	
	
}
