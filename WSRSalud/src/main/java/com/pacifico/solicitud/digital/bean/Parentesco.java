package com.pacifico.solicitud.digital.bean;

public class Parentesco {
private int CodigoParentesco;
private String Descripcion;
private int CodigoSexo;
private String CodigoParentescoPPS;

public int getCodigoParentesco() {
	return CodigoParentesco;
}
public void setCodigoParentesco(int codigoParentesco) {
	CodigoParentesco = codigoParentesco;
}
public String getDescripcion() {
	return Descripcion;
}
public void setDescripcion(String descripcion) {
	Descripcion = descripcion;
}
public int getCodigoSexo() {
	return CodigoSexo;
}
public void setCodigoSexo(int codigoSexo) {
	CodigoSexo = codigoSexo;
}
public String getCodigoParentescoPPS() {
	return CodigoParentescoPPS;
}
public void setCodigoParentescoPPS(String codigoParentescoPPS) {
	CodigoParentescoPPS = codigoParentescoPPS;
}

}
