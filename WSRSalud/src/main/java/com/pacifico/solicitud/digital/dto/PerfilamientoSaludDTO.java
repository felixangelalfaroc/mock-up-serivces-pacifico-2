package com.pacifico.solicitud.digital.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.pacifico.solicitud.digital.bean.Plan;
import com.pacifico.solicitud.digital.bean.TipoPlan;

public class PerfilamientoSaludDTO {
	@JsonProperty("Plan")
	private List<Plan> listaPlan;
	@JsonProperty("TipoPlan")
	private List<TipoPlan> listaTipoPlan;
	private int ResultCode;
	private String ResultMessage;
	
	public List<Plan> getListaPlan() {
		return listaPlan;
	}
	public void setListaPlan(List<Plan> listaPlan) {
		this.listaPlan = listaPlan;
	}
	public List<TipoPlan> getListaTipoPlan() {
		return listaTipoPlan;
	}
	public void setListaTipoPlan(List<TipoPlan> listaTipoPlan) {
		this.listaTipoPlan = listaTipoPlan;
	}
	public int getResultCode() {
		return ResultCode;
	}
	public void setResultCode(int resultCode) {
		ResultCode = resultCode;
	}
	public String getResultMessage() {
		return ResultMessage;
	}
	public void setResultMessage(String resultMessage) {
		ResultMessage = resultMessage;
	}
	

}
