package com.pacifico.solicitud.digital.dto;

import java.util.List;
import com.pacifico.solicitud.digital.bean.*;

public class GetTablasMaestrasDTO {
private List<Parametro> Parametro;
private List<TablaIndice> TablaIndice;
private List<TablaTablas> TablaTablas;
private List<Parentesco> Parentesco;
private List<Pais> Pais;
private List<Departamento> Departamento;
private List<Provincia> Provincia;
private List<Distrito> Distrito;
private List<TipoDescuento> TipoDescuento;
private List<Plan> Plan;
private List<TipoCobranza> TipoCobranza;
private List<Entidad> Entidad;
private List<TipoCuentaEntidad> TipoCuentaEntidad;
private List<CuentaEntidadValidacion> CuentaEntidadValidacion;
private List<TarjetaNoDeseada> TarjetaNoDeseada;
private List<BancoPreguntaSalud> BancoPreguntaSalud;
private int ResultCode;
private String ResultMessage;

public List<Parametro> getParametro() {
	return Parametro;
}
public void setParametro(List<Parametro> parametro) {
	Parametro = parametro;
}
public List<TablaIndice> getTablaIndice() {
	return TablaIndice;
}
public void setTablaIndice(List<TablaIndice> tablaIndice) {
	TablaIndice = tablaIndice;
}
public List<TablaTablas> getTablaTablas() {
	return TablaTablas;
}
public void setTablaTablas(List<TablaTablas> tablaTablas) {
	TablaTablas = tablaTablas;
}
public List<Parentesco> getParentesco() {
	return Parentesco;
}
public void setParentesco(List<Parentesco> parentesco) {
	Parentesco = parentesco;
}
public List<Pais> getPais() {
	return Pais;
}
public void setPais(List<Pais> pais) {
	Pais = pais;
}
public List<Departamento> getDepartamento() {
	return Departamento;
}
public void setDepartamento(List<Departamento> departamento) {
	Departamento = departamento;
}
public List<Provincia> getProvincia() {
	return Provincia;
}
public void setProvincia(List<Provincia> provincia) {
	Provincia = provincia;
}
public List<Distrito> getDistrito() {
	return Distrito;
}
public void setDistrito(List<Distrito> distrito) {
	Distrito = distrito;
}
public List<TipoDescuento> getTipoDescuento() {
	return TipoDescuento;
}
public void setTipoDescuento(List<TipoDescuento> tipoDescuento) {
	TipoDescuento = tipoDescuento;
}
public List<Plan> getPlan() {
	return Plan;
}
public void setPlan(List<Plan> plan) {
	Plan = plan;
}
public List<TipoCobranza> getTipoCobranza() {
	return TipoCobranza;
}
public void setTipoCobranza(List<TipoCobranza> tipoCobranza) {
	TipoCobranza = tipoCobranza;
}
public List<Entidad> getEntidad() {
	return Entidad;
}
public void setEntidad(List<Entidad> entidad) {
	Entidad = entidad;
}
public List<TipoCuentaEntidad> getTipoCuentaEntidad() {
	return TipoCuentaEntidad;
}
public void setTipoCuentaEntidad(List<TipoCuentaEntidad> tipoCuentaEntidad) {
	TipoCuentaEntidad = tipoCuentaEntidad;
}
public List<CuentaEntidadValidacion> getCuentaEntidadValidacion() {
	return CuentaEntidadValidacion;
}
public void setCuentaEntidadValidacion(List<CuentaEntidadValidacion> cuentaEntidadValidacion) {
	CuentaEntidadValidacion = cuentaEntidadValidacion;
}
public List<TarjetaNoDeseada> getTarjetaNoDeseada() {
	return TarjetaNoDeseada;
}
public void setTarjetaNoDeseada(List<TarjetaNoDeseada> tarjetaNoDeseada) {
	TarjetaNoDeseada = tarjetaNoDeseada;
}
public List<BancoPreguntaSalud> getBancoPreguntaSalud() {
	return BancoPreguntaSalud;
}
public void setBancoPreguntaSalud(List<BancoPreguntaSalud> bancoPreguntaSalud) {
	BancoPreguntaSalud = bancoPreguntaSalud;
}
public int getResultCode() {
	return ResultCode;
}
public void setResultCode(int resultCode) {
	ResultCode = resultCode;
}
public String getResultMessage() {
	return ResultMessage;
}
public void setResultMessage(String resultMessage) {
	ResultMessage = resultMessage;
}

} 
