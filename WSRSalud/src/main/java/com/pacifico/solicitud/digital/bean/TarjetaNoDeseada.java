package com.pacifico.solicitud.digital.bean;

public class TarjetaNoDeseada {
private int CodigoTarjetaNoDeseada;
private String Bin;
private String NombreEntidad;
public int getCodigoTarjetaNoDeseada() {
	return CodigoTarjetaNoDeseada;
}
public void setCodigoTarjetaNoDeseada(int codigoTarjetaNoDeseada) {
	CodigoTarjetaNoDeseada = codigoTarjetaNoDeseada;
}
public String getBin() {
	return Bin;
}
public void setBin(String bin) {
	Bin = bin;
}
public String getNombreEntidad() {
	return NombreEntidad;
}
public void setNombreEntidad(String nombreEntidad) {
	NombreEntidad = nombreEntidad;
}


}
