package com.pacifico.solicitud.digital.json;

public class AnularSolicitudRequest extends PerfilamientoSaludRequest{
	
	private Integer idSolicitudMultiple;

	public Integer getIdSolicitudMultiple() {
		return idSolicitudMultiple;
	}

	public void setIdSolicitudMultiple(Integer idSolicitudMultiple) {
		this.idSolicitudMultiple = idSolicitudMultiple;
	}
	
}
