package com.pacifico.solicitud.digital.bean;

public class CuentaEntidadValidacion {
private int CodigoValidacion;
private int CodigoEntidad;
private int CodigoTipoCuenta;
private int PosicionInicial;
private int PosicionFinal;
private String CaracterValidador;
private String Descripcion;
public int getCodigoValidacion() {
	return CodigoValidacion;
}
public void setCodigoValidacion(int codigoValidacion) {
	CodigoValidacion = codigoValidacion;
}
public int getCodigoEntidad() {
	return CodigoEntidad;
}
public void setCodigoEntidad(int codigoEntidad) {
	CodigoEntidad = codigoEntidad;
}
public int getCodigoTipoCuenta() {
	return CodigoTipoCuenta;
}
public void setCodigoTipoCuenta(int codigoTipoCuenta) {
	CodigoTipoCuenta = codigoTipoCuenta;
}
public int getPosicionInicial() {
	return PosicionInicial;
}
public void setPosicionInicial(int posicionInicial) {
	PosicionInicial = posicionInicial;
}
public int getPosicionFinal() {
	return PosicionFinal;
}
public void setPosicionFinal(int posicionFinal) {
	PosicionFinal = posicionFinal;
}
public String getCaracterValidador() {
	return CaracterValidador;
}
public void setCaracterValidador(String caracterValidador) {
	CaracterValidador = caracterValidador;
}
public String getDescripcion() {
	return Descripcion;
}
public void setDescripcion(String descripcion) {
	Descripcion = descripcion;
}


}
