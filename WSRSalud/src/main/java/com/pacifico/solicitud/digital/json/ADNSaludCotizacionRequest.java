package com.pacifico.solicitud.digital.json;

public class ADNSaludCotizacionRequest extends PerfilamientoSaludRequest {
	private int IdCotizacion;
	private int IdProspecto;

	public int getIdCotizacion() {
		return IdCotizacion;
	}

	public void setIdCotizacion(int idCotizacion) {
		IdCotizacion = idCotizacion;
	}

	public int getIdProspecto() {
		return IdProspecto;
	}

	public void setIdProspecto(int idProspecto) {
		IdProspecto = idProspecto;
	}

}
