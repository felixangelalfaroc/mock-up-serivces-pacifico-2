package com.pacifico.solicitud.digital.bean;

public class SolicitudMultiple {
	private Integer idSolicitudMultiple;
	private Integer codigoEstado;
	
	public Integer getIdSolicitudMultiple() {
		return idSolicitudMultiple;
	}
	public void setIdSolicitudMultiple(Integer idSolicitudMultiple) {
		this.idSolicitudMultiple = idSolicitudMultiple;
	}
	public Integer getCodigoEstado() {
		return codigoEstado;
	}
	public void setCodigoEstado(Integer codigoEstado) {
		this.codigoEstado = codigoEstado;
	}
		
}
