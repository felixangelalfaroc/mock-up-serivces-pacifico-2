package com.pacifico.solicitud.digital.json;

public class PerfilamientoSaludRequest {
	// @JsonProperty("CodigoIntermediario")
	private Integer codigoIntermediario;
	// @JsonProperty("MAC")
	private Integer mac;
	// @JsonProperty("NumeroSerie")
	private Integer numeroSerie;
	private Salud salud;

	public Integer getCodigoIntermediario() {
		return codigoIntermediario;
	}

	public void setCodigoIntermediario(Integer codigoIntermediario) {
		this.codigoIntermediario = codigoIntermediario;
	}

	public Integer getMac() {
		return mac;
	}

	public void setMac(Integer mac) {
		this.mac = mac;
	}

	public Integer getNumeroSerie() {
		return numeroSerie;
	}

	public void setNumeroSerie(Integer numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public Salud getSalud() {
		return salud;
	}

	public void setSalud(Salud salud) {
		this.salud = salud;
	}

}
