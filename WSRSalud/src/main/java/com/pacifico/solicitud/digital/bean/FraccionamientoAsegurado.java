package com.pacifico.solicitud.digital.bean;

import java.math.BigDecimal;

public class FraccionamientoAsegurado {

	private Integer idFamiliarDispositivo;
	private BigDecimal primaMensual;
	private BigDecimal primaAnual;

	public Integer getIdFamiliarDispositivo() {
		return idFamiliarDispositivo;
	}

	public void setIdFamiliarDispositivo(Integer idFamiliarDispositivo) {
		this.idFamiliarDispositivo = idFamiliarDispositivo;
	}

	public BigDecimal getPrimaMensual() {
		return primaMensual;
	}

	public void setPrimaMensual(BigDecimal primaMensual) {
		this.primaMensual = primaMensual;
	}

	public BigDecimal getPrimaAnual() {
		return primaAnual;
	}

	public void setPrimaAnual(BigDecimal primaAnual) {
		this.primaAnual = primaAnual;
	}

}
