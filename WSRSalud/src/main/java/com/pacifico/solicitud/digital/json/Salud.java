package com.pacifico.solicitud.digital.json;

public class Salud {
	private String codigoCanalPPS;
	private String flagTengoSeguro;
	private String codigoTipoCompania;
	private String codigoTipoSeguroSalud;
	private String flagUltimosDias;
	private String monedaTotalIngresoMensualFamiliar;
	private String totalIngresoMensualFamiliar;
	private String flagReembolso;
	private String flagAtencionExtranjero;
	private String idCentroMedicoPreferencia1;
	private String idCentroMedicoPreferencia2;
	private String idCentroMedicoPreferencia3;
	private String fechaNacimientoTitular;
	private String codigoTipoFamiliarTitular;
	
	public String getCodigoCanalPPS() {
		return codigoCanalPPS;
	}
	public void setCodigoCanalPPS(String codigoCanalPPS) {
		this.codigoCanalPPS = codigoCanalPPS;
	}
	public String getFlagTengoSeguro() {
		return flagTengoSeguro;
	}
	public void setFlagTengoSeguro(String flagTengoSeguro) {
		this.flagTengoSeguro = flagTengoSeguro;
	}
	public String getCodigoTipoCompania() {
		return codigoTipoCompania;
	}
	public void setCodigoTipoCompania(String codigoTipoCompania) {
		this.codigoTipoCompania = codigoTipoCompania;
	}
	public String getCodigoTipoSeguroSalud() {
		return codigoTipoSeguroSalud;
	}
	public void setCodigoTipoSeguroSalud(String codigoTipoSeguroSalud) {
		this.codigoTipoSeguroSalud = codigoTipoSeguroSalud;
	}
	public String getFlagUltimosDias() {
		return flagUltimosDias;
	}
	public void setFlagUltimosDias(String flagUltimosDias) {
		this.flagUltimosDias = flagUltimosDias;
	}
	public String getMonedaTotalIngresoMensualFamiliar() {
		return monedaTotalIngresoMensualFamiliar;
	}
	public void setMonedaTotalIngresoMensualFamiliar(
			String monedaTotalIngresoMensualFamiliar) {
		this.monedaTotalIngresoMensualFamiliar = monedaTotalIngresoMensualFamiliar;
	}
	public String getTotalIngresoMensualFamiliar() {
		return totalIngresoMensualFamiliar;
	}
	public void setTotalIngresoMensualFamiliar(String totalIngresoMensualFamiliar) {
		this.totalIngresoMensualFamiliar = totalIngresoMensualFamiliar;
	}
	public String getFlagReembolso() {
		return flagReembolso;
	}
	public void setFlagReembolso(String flagReembolso) {
		this.flagReembolso = flagReembolso;
	}
	public String getFlagAtencionExtranjero() {
		return flagAtencionExtranjero;
	}
	public void setFlagAtencionExtranjero(String flagAtencionExtranjero) {
		this.flagAtencionExtranjero = flagAtencionExtranjero;
	}
	public String getIdCentroMedicoPreferencia1() {
		return idCentroMedicoPreferencia1;
	}
	public void setIdCentroMedicoPreferencia1(String idCentroMedicoPreferencia1) {
		this.idCentroMedicoPreferencia1 = idCentroMedicoPreferencia1;
	}
	public String getIdCentroMedicoPreferencia2() {
		return idCentroMedicoPreferencia2;
	}
	public void setIdCentroMedicoPreferencia2(String idCentroMedicoPreferencia2) {
		this.idCentroMedicoPreferencia2 = idCentroMedicoPreferencia2;
	}
	public String getIdCentroMedicoPreferencia3() {
		return idCentroMedicoPreferencia3;
	}
	public void setIdCentroMedicoPreferencia3(String idCentroMedicoPreferencia3) {
		this.idCentroMedicoPreferencia3 = idCentroMedicoPreferencia3;
	}
	public String getFechaNacimientoTitular() {
		return fechaNacimientoTitular;
	}
	public void setFechaNacimientoTitular(String fechaNacimientoTitular) {
		this.fechaNacimientoTitular = fechaNacimientoTitular;
	}
	public String getCodigoTipoFamiliarTitular() {
		return codigoTipoFamiliarTitular;
	}
	public void setCodigoTipoFamiliarTitular(String codigoTipoFamiliarTitular) {
		this.codigoTipoFamiliarTitular = codigoTipoFamiliarTitular;
	}	
	
}
