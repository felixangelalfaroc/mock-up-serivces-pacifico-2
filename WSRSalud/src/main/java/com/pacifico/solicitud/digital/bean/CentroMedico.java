package com.pacifico.solicitud.digital.bean;

public class CentroMedico {
	private Integer idCentroMedico;
	private String nombre;

	public Integer getIdCentroMedico() {
		return idCentroMedico;
	}

	public void setIdCentroMedico(Integer idCentroMedico) {
		this.idCentroMedico = idCentroMedico;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

}
