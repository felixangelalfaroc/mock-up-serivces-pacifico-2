package com.pacifico.solicitud.digital.bean;

public class DatosError {
	private String NombreAplicacion;
	private String DescripcionError;	
	private String fecha;
	
	public String getNombreAplicacion() {
		return NombreAplicacion;
	}
	public void setNombreAplicacion(String nombreAplicacion) {
		NombreAplicacion = nombreAplicacion;
	}
	public String getDescripcionError() {
		return DescripcionError;
	}
	public void setDescripcionError(String descripcionError) {
		DescripcionError = descripcionError;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
}
