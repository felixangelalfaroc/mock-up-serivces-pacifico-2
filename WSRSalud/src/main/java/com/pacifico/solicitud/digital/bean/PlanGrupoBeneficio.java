package com.pacifico.solicitud.digital.bean;

public class PlanGrupoBeneficio {
	private Integer idGrupoBeneficio;
	private String descripcion;
	private String observacion;
	private RedMedica redMedica;

	public Integer getIdGrupoBeneficio() {
		return idGrupoBeneficio;
	}

	public void setIdGrupoBeneficio(Integer idGrupoBeneficio) {
		this.idGrupoBeneficio = idGrupoBeneficio;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public RedMedica getRedMedica() {
		return redMedica;
	}

	public void setRedMedica(RedMedica redMedica) {
		this.redMedica = redMedica;
	}

}
