package com.pacifico.solicitud.digital.service;

import com.pacifico.solicitud.digital.dto.ADNSaludCotizacionDTO;
import com.pacifico.solicitud.digital.dto.AnularSolicitudDTO;
import com.pacifico.solicitud.digital.dto.CotizacionSaludDTO;
import com.pacifico.solicitud.digital.dto.GetTablasMaestrasDTO;
import com.pacifico.solicitud.digital.dto.PerfilamientoSaludDTO;
import com.pacifico.solicitud.digital.dto.ProspectoDTO;
import com.pacifico.solicitud.digital.json.AnularSolicitudRequest;
import com.pacifico.solicitud.digital.json.CotizacionSaludRequest;
import com.pacifico.solicitud.digital.json.PerfilamientoSaludRequest;



public interface SolicitudDigitalService {
	
	public PerfilamientoSaludDTO getPerfilamientoSalud(PerfilamientoSaludRequest perfilamientoSaludRequest);
	public CotizacionSaludDTO getCotizacionSalud(CotizacionSaludRequest cotizacionSaludRequest); 
	public GetTablasMaestrasDTO getTablasMaestras(); 
	public AnularSolicitudDTO setSolicitudMultiple(AnularSolicitudRequest anularSolicitudRequest);
	public ProspectoDTO getProspecto();
	public ADNSaludCotizacionDTO getAdnSolicitudCotizacion();
}
