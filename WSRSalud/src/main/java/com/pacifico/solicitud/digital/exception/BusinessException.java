package com.pacifico.solicitud.digital.exception;

public class BusinessException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	private String code;

	public BusinessException() {
		super();
	}

	public BusinessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public BusinessException(String message, Throwable cause) {
		super(message, cause);
	}

	public BusinessException(String code, String message) {
		super(message);
		this.code = code;
	}

	public BusinessException(Throwable cause) {
		super(cause);
	}

	public String getCode() {
		return code;
	}
}