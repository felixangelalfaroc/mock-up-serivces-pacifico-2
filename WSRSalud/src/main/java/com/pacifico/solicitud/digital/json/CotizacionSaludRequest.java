package com.pacifico.solicitud.digital.json;

import java.util.List;

import com.pacifico.solicitud.digital.bean.Asegurado;
import com.pacifico.solicitud.digital.bean.Plan;

public class CotizacionSaludRequest extends PerfilamientoSaludRequest {

	private String codigoCanalPPS;
	private List<Asegurado> listaAsegurado;
	private List<Plan> listaPlan;

	public String getCodigoCanalPPS() {
		return codigoCanalPPS;
	}

	public void setCodigoCanalPPS(String codigoCanalPPS) {
		this.codigoCanalPPS = codigoCanalPPS;
	}

	public List<Asegurado> getListaAsegurado() {
		return listaAsegurado;
	}

	public void setListaAsegurado(List<Asegurado> listaAsegurado) {
		this.listaAsegurado = listaAsegurado;
	}

	public List<Plan> getListaPlan() {
		return listaPlan;
	}

	public void setListaPlan(List<Plan> listaPlan) {
		this.listaPlan = listaPlan;
	}
	
}
