package com.pacifico.solicitud.digital.bean;

public class TipoCuentaEntidad {
private int CodigoEntidad;
private int CodigoTipoCuenta;
private String descripcion;
private String PlantillNumeroCuenta;
private int CodigoMoneda;
public int getCodigoEntidad() {
	return CodigoEntidad;
}
public void setCodigoEntidad(int codigoEntidad) {
	CodigoEntidad = codigoEntidad;
}
public int getCodigoTipoCuenta() {
	return CodigoTipoCuenta;
}
public void setCodigoTipoCuenta(int codigoTipoCuenta) {
	CodigoTipoCuenta = codigoTipoCuenta;
}
public String getDescripcion() {
	return descripcion;
}
public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}
public String getPlantillNumeroCuenta() {
	return PlantillNumeroCuenta;
}
public void setPlantillNumeroCuenta(String plantillNumeroCuenta) {
	PlantillNumeroCuenta = plantillNumeroCuenta;
}
public int getCodigoMoneda() {
	return CodigoMoneda;
}
public void setCodigoMoneda(int codigoMoneda) {
	CodigoMoneda = codigoMoneda;
}



}
