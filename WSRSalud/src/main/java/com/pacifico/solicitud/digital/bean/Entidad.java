package com.pacifico.solicitud.digital.bean;

public class Entidad {
private int CodigoEntidad;
private int CodigoTipoCobranza;
private String descripcion;
public int getCodigoEntidad() {
	return CodigoEntidad;
}
public void setCodigoEntidad(int codigoEntidad) {
	CodigoEntidad = codigoEntidad;
}
public int getCodigoTipoCobranza() {
	return CodigoTipoCobranza;
}
public void setCodigoTipoCobranza(int codigoTipoCobranza) {
	CodigoTipoCobranza = codigoTipoCobranza;
}
public String getDescripcion() {
	return descripcion;
}
public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}


}
