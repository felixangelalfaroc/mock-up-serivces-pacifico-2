package com.pacifico.solicitud.digital.bean;

public class Provincia {
private int CodigoProvincia;
private int CodigoDepartamento;
private String Descripcion;
public int getCodigoProvincia() {
	return CodigoProvincia;
}
public void setCodigoProvincia(int codigoProvincia) {
	CodigoProvincia = codigoProvincia;
}
public int getCodigoDepartamento() {
	return CodigoDepartamento;
}
public void setCodigoDepartamento(int codigoDepartamento) {
	CodigoDepartamento = codigoDepartamento;
}
public String getDescripcion() {
	return Descripcion;
}
public void setDescripcion(String descripcion) {
	Descripcion = descripcion;
}


}
