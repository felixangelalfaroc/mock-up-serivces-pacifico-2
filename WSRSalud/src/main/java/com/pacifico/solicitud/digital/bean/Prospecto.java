package com.pacifico.solicitud.digital.bean;

public class Prospecto {
	private int IdProspecto;
	private String NombreCompleto;
	private String Nombres;
	private String ApellidoPaterno;
	private String ApellidoMaterno;
	private int TipoProspecto;
	private CotizacionSolicitud cotizacionSolicitud;

	public int getIdProspecto() {
		return IdProspecto;
	}

	public void setIdProspecto(int idProspecto) {
		IdProspecto = idProspecto;
	}

	public String getNombreCompleto() {
		return NombreCompleto;
	}

	public void setNombreCompleto(String nombreCompleto) {
		NombreCompleto = nombreCompleto;
	}

	public String getNombres() {
		return Nombres;
	}

	public void setNombres(String nombres) {
		Nombres = nombres;
	}

	public String getApellidoPaterno() {
		return ApellidoPaterno;
	}

	public void setApellidoPaterno(String apellidoPaterno) {
		ApellidoPaterno = apellidoPaterno;
	}

	public String getApellidoMaterno() {
		return ApellidoMaterno;
	}

	public void setApellidoMaterno(String apellidoMaterno) {
		ApellidoMaterno = apellidoMaterno;
	}

	public int getTipoProspecto() {
		return TipoProspecto;
	}

	public void setTipoProspecto(int tipoProspecto) {
		TipoProspecto = tipoProspecto;
	}

	public CotizacionSolicitud getCotizacionSolicitud() {
		return cotizacionSolicitud;
	}

	public void setCotizacionSolicitud(CotizacionSolicitud cotizacionSolicitud) {
		this.cotizacionSolicitud = cotizacionSolicitud;
	}
}