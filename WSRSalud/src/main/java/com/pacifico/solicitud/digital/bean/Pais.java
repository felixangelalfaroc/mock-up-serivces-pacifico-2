package com.pacifico.solicitud.digital.bean;

public class Pais {
private int codigoPais;
private String descripcion;
private String nacionalidad;
public int getCodigoPais() {
	return codigoPais;
}
public void setCodigoPais(int codigoPais) {
	this.codigoPais = codigoPais;
}
public String getDescripcion() {
	return descripcion;
}
public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}
public String getNacionalidad() {
	return nacionalidad;
}
public void setNacionalidad(String nacionalidad) {
	this.nacionalidad = nacionalidad;
}


}
