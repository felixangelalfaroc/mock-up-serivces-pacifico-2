package com.pacifico.solicitud.digital.bean;

public class Departamento {
private int CodigoDepartamento;
private String Descripcion;
private String PrefijoTelefonico;
public int getCodigoDepartamento() {
	return CodigoDepartamento;
}
public void setCodigoDepartamento(int codigoDepartamento) {
	CodigoDepartamento = codigoDepartamento;
}
public String getDescripcion() {
	return Descripcion;
}
public void setDescripcion(String descripcion) {
	Descripcion = descripcion;
}
public String getPrefijoTelefonico() {
	return PrefijoTelefonico;
}
public void setPrefijoTelefonico(String prefijoTelefonico) {
	PrefijoTelefonico = prefijoTelefonico;
}

}
