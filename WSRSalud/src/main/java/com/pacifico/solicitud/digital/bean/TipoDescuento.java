package com.pacifico.solicitud.digital.bean;

public class TipoDescuento {
private int IdTipoDescuento;
private String Descripcion;
private String CodigoDescuentoPPS;
private boolean FlagAdjuntarFotoCheck;

public int getIdTipoDescuento() {
	return IdTipoDescuento;
}
public void setIdTipoDescuento(int idTipoDescuento) {
	IdTipoDescuento = idTipoDescuento;
}
public String getDescripcion() {
	return Descripcion;
}
public void setDescripcion(String descripcion) {
	Descripcion = descripcion;
}
public String getCodigoDescuentoPPS() {
	return CodigoDescuentoPPS;
}
public void setCodigoDescuentoPPS(String codigoDescuentoPPS) {
	CodigoDescuentoPPS = codigoDescuentoPPS;
}
public boolean getFlagAdjuntarFotoCheck() {
	return FlagAdjuntarFotoCheck;
}
public void setFlagAdjuntarFotoCheck(boolean flagAdjuntarFotoCheck) {
	FlagAdjuntarFotoCheck = flagAdjuntarFotoCheck;
}

}
