package com.pacifico.solicitud.digital.bean;

import java.math.BigDecimal;
import java.util.Date;

public class Asegurado {
	
	private Integer idFamiliarDispositivo;
	private String tipoDocumento;
	private String numeroDocumento;
	private String codigoSexo;
	private Date fechaNacimiento;
	private BigDecimal talla;
	private BigDecimal peso;
	private Recargo recargo;
	
	public Integer getIdFamiliarDispositivo() {
		return idFamiliarDispositivo;
	}
	public void setIdFamiliarDispositivo(Integer idFamiliarDispositivo) {
		this.idFamiliarDispositivo = idFamiliarDispositivo;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	public void setNumeroDocumento(String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	public String getCodigoSexo() {
		return codigoSexo;
	}
	public void setCodigoSexo(String codigoSexo) {
		this.codigoSexo = codigoSexo;
	}
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public BigDecimal getTalla() {
		return talla;
	}
	public void setTalla(BigDecimal talla) {
		this.talla = talla;
	}
	public BigDecimal getPeso() {
		return peso;
	}
	public void setPeso(BigDecimal peso) {
		this.peso = peso;
	}
	public Recargo getRecargo() {
		return recargo;
	}
	public void setRecargo(Recargo recargo) {
		this.recargo = recargo;
	}
}
	
	

	
	
