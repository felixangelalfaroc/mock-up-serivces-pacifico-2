package com.pacifico.solicitud.digital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pacifico.solicitud.digital.dto.ProspectoDTO;
import com.pacifico.solicitud.digital.service.SolicitudDigitalService;

@RestController
@RequestMapping("prospecto")
public class ProspectoController {

	@Autowired
	private SolicitudDigitalService solicitudDigitalService;

	@RequestMapping(method = RequestMethod.GET)
	public ProspectoDTO obtenerPerfilamientoSalud() {
		
			ProspectoDTO dummy = new ProspectoDTO();
			dummy=solicitudDigitalService.getProspecto();
			
		return dummy;

	}

}
