package com.pacifico.solicitud.digital.exception;

public class GenericException extends RuntimeException {
	
	private static final long serialVersionUID = 7109216192740041009L;

	private static final int DEFAULT_STATUS = 500;
	
	private String code;
	private int status;

	public GenericException() {
		super();
	}

	public GenericException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public GenericException(String message, Throwable cause) {
		super(message, cause);
	}

	public GenericException(String code, String message) {
		super(message);
		this.status = DEFAULT_STATUS;
		this.code = code;
	}
	
	public GenericException(int status, String code, String message) {
		super(message);
		this.status = status;
		this.code = code;
	}

	public GenericException(Throwable cause) {
		super(cause);
	}

	public String getCode() {
		return code;
	}

	public int getStatus() {
		return status;
	}
}