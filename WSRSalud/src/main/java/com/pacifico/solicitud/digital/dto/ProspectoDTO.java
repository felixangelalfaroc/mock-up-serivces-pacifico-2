package com.pacifico.solicitud.digital.dto;

import java.util.List;

import com.pacifico.solicitud.digital.bean.*;

public class ProspectoDTO {
	private List<Prospecto> listaProspecto;
	private int ResultCode;
	private String ResultMessage;

	public List<Prospecto> getListaProspecto() {
		return listaProspecto;
	}

	public void setListaProspecto(List<Prospecto> listaProspecto) {
		this.listaProspecto = listaProspecto;
	}

	public int getResultCode() {
		return ResultCode;
	}

	public void setResultCode(int resultCode) {
		ResultCode = resultCode;
	}

	public String getResultMessage() {
		return ResultMessage;
	}

	public void setResultMessage(String resultMessage) {
		ResultMessage = resultMessage;
	}

}
