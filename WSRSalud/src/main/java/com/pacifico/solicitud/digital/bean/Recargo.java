package com.pacifico.solicitud.digital.bean;

public class Recargo {
	private String codReca;
	private String factor;	

	public String getCodReca() {
		return codReca;
	}

	public void setCodReca(String codReca) {
		this.codReca = codReca;
	}

	public String getFactor() {
		return factor;
	}

	public void setFactor(String factor) {
		this.factor = factor;
	}

}
