package com.pacifico.solicitud.digital.controller;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pacifico.solicitud.digital.dto.PerfilamientoSaludDTO;
import com.pacifico.solicitud.digital.json.PerfilamientoSaludRequest;
import com.pacifico.solicitud.digital.service.SolicitudDigitalService;

@RestController
@RequestMapping("perfilamientoSalud")
public class PerfilamientoSaludController {

	@Autowired
	private SolicitudDigitalService solicitudDigitalService;

	@RequestMapping(method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON,produces=MediaType.APPLICATION_JSON)
	public PerfilamientoSaludDTO obtenerPerfilamientoSalud(@RequestBody PerfilamientoSaludRequest perfilamientoSaludRequest) {
		
			PerfilamientoSaludDTO perfilamientoSaludDTO= new PerfilamientoSaludDTO();
			perfilamientoSaludDTO= solicitudDigitalService.getPerfilamientoSalud(perfilamientoSaludRequest);

		return perfilamientoSaludDTO;
	}

}
