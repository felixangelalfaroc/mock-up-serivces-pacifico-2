package com.pacifico.solicitud.digital.bean;

import java.math.BigDecimal;

public class Fraccionamiento {
	private String codPlanFrac;
	private String desPlanFrac;
	private String verPlanFrac;
	private String indDebito;
	private String indAsesor;
	private BigDecimal montTotCuotaMen;
	private BigDecimal MontTotCuotaAnu;
	private FraccionamientoAsegurado fraccionamientoAsegurado;
	private Observacion observacion;
	
	public String getCodPlanFrac() {
		return codPlanFrac;
	}
	public void setCodPlanFrac(String codPlanFrac) {
		this.codPlanFrac = codPlanFrac;
	}
	public String getDesPlanFrac() {
		return desPlanFrac;
	}
	public void setDesPlanFrac(String desPlanFrac) {
		this.desPlanFrac = desPlanFrac;
	}
	public String getVerPlanFrac() {
		return verPlanFrac;
	}
	public void setVerPlanFrac(String verPlanFrac) {
		this.verPlanFrac = verPlanFrac;
	}
	public String getIndDebito() {
		return indDebito;
	}
	public void setIndDebito(String indDebito) {
		this.indDebito = indDebito;
	}
	public String getIndAsesor() {
		return indAsesor;
	}
	public void setIndAsesor(String indAsesor) {
		this.indAsesor = indAsesor;
	}
	public BigDecimal getMontTotCuotaMen() {
		return montTotCuotaMen;
	}
	public void setMontTotCuotaMen(BigDecimal montTotCuotaMen) {
		this.montTotCuotaMen = montTotCuotaMen;
	}
	public BigDecimal getMontTotCuotaAnu() {
		return MontTotCuotaAnu;
	}
	public void setMontTotCuotaAnu(BigDecimal montTotCuotaAnu) {
		MontTotCuotaAnu = montTotCuotaAnu;
	}
	public FraccionamientoAsegurado getFraccionamientoAsegurado() {
		return fraccionamientoAsegurado;
	}
	public void setFraccionamientoAsegurado(
			FraccionamientoAsegurado fraccionamientoAsegurado) {
		this.fraccionamientoAsegurado = fraccionamientoAsegurado;
	}
	public Observacion getObservacion() {
		return observacion;
	}
	public void setObservacion(Observacion observacion) {
		this.observacion = observacion;
	}
	

}
