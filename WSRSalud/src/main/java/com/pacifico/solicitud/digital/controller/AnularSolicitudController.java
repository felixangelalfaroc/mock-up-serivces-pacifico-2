package com.pacifico.solicitud.digital.controller;

import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pacifico.solicitud.digital.dto.AnularSolicitudDTO;
import com.pacifico.solicitud.digital.json.AnularSolicitudRequest;
import com.pacifico.solicitud.digital.service.SolicitudDigitalService;


@RestController
@RequestMapping("anularSolicitud")
public class AnularSolicitudController {

	@Autowired
	private SolicitudDigitalService solicitudDigitalService;
	
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
	public AnularSolicitudDTO setAnularSolicitud(@RequestBody AnularSolicitudRequest anularSolicitudRequest){
		
			AnularSolicitudDTO anularSolicitudDTO = new AnularSolicitudDTO();
			anularSolicitudDTO=solicitudDigitalService.setSolicitudMultiple(anularSolicitudRequest);
		
			return anularSolicitudDTO;
		
	}

}
