package com.pacifico.solicitud.digital.bean;

public class BancoPreguntaSalud {
private int IdBancoPreguntaSalud;
private String CodigoProducto;
private String DescripcionPregunta;
private boolean AplicaMasculino;
private boolean AplicaFemenino;
private int TipoRespuesta;
public int getIdBancoPreguntaSalud() {
	return IdBancoPreguntaSalud;
}
public void setIdBancoPreguntaSalud(int idBancoPreguntaSalud) {
	IdBancoPreguntaSalud = idBancoPreguntaSalud;
}
public String getCodigoProducto() {
	return CodigoProducto;
}
public void setCodigoProducto(String codigoProducto) {
	CodigoProducto = codigoProducto;
}
public String getDescripcionPregunta() {
	return DescripcionPregunta;
}
public void setDescripcionPregunta(String descripcionPregunta) {
	DescripcionPregunta = descripcionPregunta;
}
public boolean isAplicaMasculino() {
	return AplicaMasculino;
}
public void setAplicaMasculino(boolean aplicaMasculino) {
	AplicaMasculino = aplicaMasculino;
}
public boolean isAplicaFemenino() {
	return AplicaFemenino;
}
public void setAplicaFemenino(boolean aplicaFemenino) {
	AplicaFemenino = aplicaFemenino;
}
public int getTipoRespuesta() {
	return TipoRespuesta;
}
public void setTipoRespuesta(int tipoRespuesta) {
	TipoRespuesta = tipoRespuesta;
}

}
