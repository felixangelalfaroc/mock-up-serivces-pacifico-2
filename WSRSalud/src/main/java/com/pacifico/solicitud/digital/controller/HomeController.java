package com.pacifico.solicitud.digital.controller;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class HomeController {
	
	public static final String WS = "WSRSalud";
	private static final Logger LOGGER = Logger.getLogger(HomeController.class);

	/**
	  * @name Home
	  * @author INDRA
	  * @purpose muestra pagina por defecto
	  * @param 
	  * @return 
	  * @throws 
	  */	
	@RequestMapping(method = RequestMethod.GET)
	public String home() {
		LOGGER.info(WS);
		return WS;
	}
}