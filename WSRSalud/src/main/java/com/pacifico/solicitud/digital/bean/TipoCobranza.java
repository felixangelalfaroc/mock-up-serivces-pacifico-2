package com.pacifico.solicitud.digital.bean;

public class TipoCobranza {
private int CodigoTipoCobranza;
private String Descripcion;
private boolean AplicaPrimerPago;
private boolean AplicaPagoRecurrente;
public int getCodigoTipoCobranza() {
	return CodigoTipoCobranza;
}
public void setCodigoTipoCobranza(int codigoTipoCobranza) {
	CodigoTipoCobranza = codigoTipoCobranza;
}
public String getDescripcion() {
	return Descripcion;
}
public void setDescripcion(String descripcion) {
	Descripcion = descripcion;
}
public boolean isAplicaPrimerPago() {
	return AplicaPrimerPago;
}
public void setAplicaPrimerPago(boolean aplicaPrimerPago) {
	AplicaPrimerPago = aplicaPrimerPago;
}
public boolean isAplicaPagoRecurrente() {
	return AplicaPagoRecurrente;
}
public void setAplicaPagoRecurrente(boolean aplicaPagoRecurrente) {
	AplicaPagoRecurrente = aplicaPagoRecurrente;
}

}
