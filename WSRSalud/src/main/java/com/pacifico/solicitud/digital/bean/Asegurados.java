package com.pacifico.solicitud.digital.bean;

public class Asegurados {
	private Integer idFamiliarDispositivo;
	private Observacion observacion;

	public Integer getIdFamiliarDispositivo() {
		return idFamiliarDispositivo;
	}

	public void setIdFamiliarDispositivo(Integer idFamiliarDispositivo) {
		this.idFamiliarDispositivo = idFamiliarDispositivo;
	}

	public Observacion getObservacion() {
		return observacion;
	}

	public void setObservacion(Observacion observacion) {
		this.observacion = observacion;
	}

}
