package com.pacifico.solicitud.digital.bean;

public class Parametro {
private int idParametro;
private String descripcion;
private String valorCadena;
private double valorNumerico;

public int getIdParametro() {
	return idParametro;
}
public void setIdParametro(int idParametro) {
	this.idParametro = idParametro;
}
public String getDescripcion() {
	return descripcion;
}
public void setDescripcion(String descripcion) {
	this.descripcion = descripcion;
}
public String getValorCadena() {
	return valorCadena;
}
public void setValorCadena(String valorCadena) {
	this.valorCadena = valorCadena;
}
public double getValorNumerico() {
	return valorNumerico;
}
public void setValorNumerico(double valorNumerico) {
	this.valorNumerico = valorNumerico;
}

}
