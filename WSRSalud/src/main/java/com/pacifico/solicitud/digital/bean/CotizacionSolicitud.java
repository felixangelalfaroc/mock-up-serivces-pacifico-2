package com.pacifico.solicitud.digital.bean;

public class CotizacionSolicitud {
private int IdCotizacion;
private int IdSolicitudMultiple;
private int CodigoEstadoSolicitudMultiple;
public int getIdCotizacion() {
	return IdCotizacion;
}
public void setIdCotizacion(int idCotizacion) {
	IdCotizacion = idCotizacion;
}
public int getIdSolicitudMultiple() {
	return IdSolicitudMultiple;
}
public void setIdSolicitudMultiple(int idSolicitudMultiple) {
	IdSolicitudMultiple = idSolicitudMultiple;
}
public int getCodigoEstadoSolicitudMultiple() {
	return CodigoEstadoSolicitudMultiple;
}
public void setCodigoEstadoSolicitudMultiple(int codigoEstadoSolicitudMultiple) {
	CodigoEstadoSolicitudMultiple = codigoEstadoSolicitudMultiple;
}

}
