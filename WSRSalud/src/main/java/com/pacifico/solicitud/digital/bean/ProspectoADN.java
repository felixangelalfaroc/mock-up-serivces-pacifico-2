package com.pacifico.solicitud.digital.bean;

public class ProspectoADN {
private int IdProspecto;
private String Nombres;
private String ApellidoPaterno;
private String ApellidoMaterno;
private int CodigoEstadoCivil;
private int CodigoTipoDocumento;
private String NumeroDocumento;
private String TelefonoCelular;
private String CorreoElectronico1;
private int CodigoNacionalidad;
public int getIdProspecto() {
	return IdProspecto;
}
public void setIdProspecto(int idProspecto) {
	IdProspecto = idProspecto;
}
public String getNombres() {
	return Nombres;
}
public void setNombres(String nombres) {
	Nombres = nombres;
}
public String getApellidoPaterno() {
	return ApellidoPaterno;
}
public void setApellidoPaterno(String apellidoPaterno) {
	ApellidoPaterno = apellidoPaterno;
}
public String getApellidoMaterno() {
	return ApellidoMaterno;
}
public void setApellidoMaterno(String apellidoMaterno) {
	ApellidoMaterno = apellidoMaterno;
}
public int getCodigoEstadoCivil() {
	return CodigoEstadoCivil;
}
public void setCodigoEstadoCivil(int codigoEstadoCivil) {
	CodigoEstadoCivil = codigoEstadoCivil;
}
public int getCodigoTipoDocumento() {
	return CodigoTipoDocumento;
}
public void setCodigoTipoDocumento(int codigoTipoDocumento) {
	CodigoTipoDocumento = codigoTipoDocumento;
}
public String getNumeroDocumento() {
	return NumeroDocumento;
}
public void setNumeroDocumento(String numeroDocumento) {
	NumeroDocumento = numeroDocumento;
}
public String getTelefonoCelular() {
	return TelefonoCelular;
}
public void setTelefonoCelular(String telefonoCelular) {
	TelefonoCelular = telefonoCelular;
}
public String getCorreoElectronico1() {
	return CorreoElectronico1;
}
public void setCorreoElectronico1(String correoElectronico1) {
	CorreoElectronico1 = correoElectronico1;
}
public int getCodigoNacionalidad() {
	return CodigoNacionalidad;
}
public void setCodigoNacionalidad(int codigoNacionalidad) {
	CodigoNacionalidad = codigoNacionalidad;
}

}
