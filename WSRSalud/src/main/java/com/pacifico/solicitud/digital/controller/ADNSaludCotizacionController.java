package com.pacifico.solicitud.digital.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.pacifico.solicitud.digital.dto.ADNSaludCotizacionDTO;
import com.pacifico.solicitud.digital.service.SolicitudDigitalService;

@RestController
@RequestMapping("adnSaludCotizacion")
public class ADNSaludCotizacionController {

	@Autowired
	private SolicitudDigitalService solicitudDigitalService;

	@RequestMapping(method = RequestMethod.GET)
	public ADNSaludCotizacionDTO obtenerAdnSaludCotizacion() {
			ADNSaludCotizacionDTO dummy = new ADNSaludCotizacionDTO();
			dummy=solicitudDigitalService.getAdnSolicitudCotizacion();

		return dummy;

	}
	
}
